import unittest
import json
from proxy_app.app import create_app
# from proxy_app.controllers.helpers import create_clean_response
import responses


USER_AGENT = 'Test'


def generate_get_json_resp(ip):
    return {
          "args": {},
          "headers": {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Host": "httpbin.org",
            "User-Agent": USER_AGENT
          },
          "origin": f"{ip}, {ip}",
          "url": "http://httpbin.org/get"
        }


def generate_post_json_resp(ip):
    return {
          "args": {},
          "data": "",
          "files": {},
          "form": {
            "asdf": "blah"
          },
          "headers": {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Content-Length": "9",
            "Content-Type": "application/x-www-form-urlencoded",
            "Host": "httpbin.org",
            "User-Agent": USER_AGENT
          },
          "json": None,
          "origin": f"{ip}, {ip}",
          "url": "http://httpbin.org/post"
        }


class TestProxy(unittest.TestCase):
    def setUp(self):
        self.app = create_app().test_client()
        # Valid if IP is not dynamic
        self.ip = "127.1.1.0"

    def create_get_response(self):
        url = "http://httpbin.org/get"
        responses.add(
            method=responses.GET,
            url=url,
            json=generate_get_json_resp(self.ip),
            status=200
        )

    @responses.activate
    def test_get(self):
        get_url = 'http://localhost:8000/ proxy/http://httpbin.org/get'
        expected_response = generate_get_json_resp(self.ip)
        self.create_get_response()

        response = self.app.get(
            get_url, environ_base={'HTTP_USER_AGENT': USER_AGENT}
        )
        response = response.data.decode("utf-8")
        response = json.loads(response)
        self.assertDictEqual(expected_response, response)

    def create_post_response(self):
        url = "http://httpbin.org/post"
        responses.add(
            method=responses.POST,
            url=url,
            json=generate_post_json_resp(self.ip),
            status=200
        )

    @responses.activate
    def test_post(self):
        post_url = 'http://localhost:8000/ proxy/http://httpbin.org/post'
        expected_response = generate_post_json_resp(self.ip)
        self.create_post_response()

        response = self.app.post(
            post_url,
            environ_base={'HTTP_USER_AGENT': USER_AGENT},
            data={"asdf": "blah"}
        )
        response = response.data.decode('utf-8')
        response = json.loads(response)
        self.assertDictEqual(expected_response, response)
