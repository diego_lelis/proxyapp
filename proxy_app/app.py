from flask import Flask


def create_app():
    """Creates Flask app"""

    my_app = Flask(__name__)
    from proxy_app.controllers.proxy import proxy
    my_app.register_blueprint(proxy)
    return my_app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True, host='0.0.0.0', port=8000)
