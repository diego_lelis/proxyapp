# Proxy App

## Getting started:

1. **Creating venv:** virtualenv -p python3 env && source env/bin/activate
2. **Install:** pip install -r requirements.txt
3. **Running app:** ./start_app.sh
4. **Test:** ./test_app.sh
5. **Test coverage:** proxy_app/htmlcov/index.html


## Using the application
**GET** command: curl  http://localhost:8000/%20proxy/http://httpbin.org/get

**POST** command: curl -X POST -d asdf=blah  http://localhost:8000/%20proxy/http://httpbin.org/post

### FYI 
This application uses git-hooks to perform code-sanity checks
