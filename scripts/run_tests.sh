#!/usr/bin/env bash

# Returns error
set -e

# Go to directory above (app directory)
cd "${0%/*}/.."

echo "Running tests code quality check"
flake8 . \
    --exclude .git,__pycache__,myenv,env,.pytest_cache

echo "Running tests"
cd proxy_app && pytest --cov=proxy_app --cov-report html