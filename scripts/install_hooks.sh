#!/usr/bin/env bash

GIT_DIR=$(git rev-parse --git-dir)

rm $GIT_DIR/hooks/pre-commit

echo "Installing hooks..."
# Creates link between script and pre-commit
ln -s ../../scripts/pre_commit.sh $GIT_DIR/hooks/pre-commit
echo "Done!"
