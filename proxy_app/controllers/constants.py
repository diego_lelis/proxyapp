SUCCESS_CODE = 200
FAIL_CODE = 400

EXPECT_RESP_FORM_GET = \
    {
      "args": {},
      "headers": {
        "Accept": "accept",
        "Accept-Encoding": "encoding",
        "Host": "host",
        "User-Agent": "agent"
      },
      "origin": "ip",
      "url": "url"
    }


EXPECT_RESP_FORM_POST = \
    {
        "args": {},
        "data": "",
        "files": {},
        "form": {
            "asdf": "asdf"
        },
        "headers": {
            "Accept": "accept",
            "Accept-Encoding": "encoding",
            "Content-Length": "length",
            "Content-Type": "type",
            "Host": "host",
            "User-Agent": "agent"
        },
        "json": None,
        "origin": "ip",
        "url": "url"
    }
