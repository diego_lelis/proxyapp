from flask import Blueprint
from flask import request
import requests
# from .constants import EXPECT_RESP_FORM_GET  # Cleans response
# from .constants import EXPECT_RESP_FORM_POST  # Cleans response
from .constants import SUCCESS_CODE
from .constants import FAIL_CODE
# from .helpers import create_clean_response  # Cleans response


def create_headers(req):
    """Creates a header without the host
    key to redirect to another endpoint"""
    headers = dict(req.headers)
    del(headers['Host'])
    return headers


def proxy_get(url, req):
    """Redirects the received requisition to
    the url passed if method is GET"""

    headers = create_headers(req)
    res = requests.get(url, headers=headers, allow_redirects=False)
    res = res.text
    return res


def proxy_post(url, req):
    """Redirects the received requisition
    to the url passed if method is POST"""

    headers = create_headers(req)
    res = requests.post(
        url,
        data=req.form,
        headers=headers,
        allow_redirects=False
    )
    res = res.text
    return res


proxy = Blueprint('proxy', __name__)


@proxy.route('/ proxy/<path:url>', methods=['GET', 'POST'])
def proxy_func(url):
    """Selects to what function requisition
    should be redirected according to method"""

    try:
        if request.method == 'GET':
            return proxy_get(url, request), SUCCESS_CODE
        elif request.method == 'POST':
            return proxy_post(url, request), SUCCESS_CODE
    except Exception as error:
        return {'Error': True, 'Message': error}, FAIL_CODE
