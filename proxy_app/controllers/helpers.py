import json


def clean_response(res_dict, expect_res_dict, res_dict_aux):
    """Creates a new dictionary with only the
    keys of the first dictionary that are present on the second one
    (update function)
    """

    for key in res_dict.keys():
        if type(res_dict[key]) != dict and key in expect_res_dict:
            res_dict_aux[key] = res_dict.get(key)
        elif key not in expect_res_dict:
            pass
        else:
            res_dict_aux[key] = {}
            clean_response(
                res_dict.get(key),
                expect_res_dict.get(key),
                res_dict_aux.get(key)
            )


def create_clean_response(res, expected_res):
    """Wrapper of the function clean_response"""

    res_aux = {}
    clean_response(json.loads(res), expected_res, res_aux)
    return json.dumps(res_aux)
